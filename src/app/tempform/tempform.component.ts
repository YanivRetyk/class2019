import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-tempform',
  templateUrl: './tempform.component.html',
  styleUrls: ['./tempform.component.css']
})
export class TempformComponent implements OnInit {

  constructor(private router: Router) { }

  cities:object[] = [{id:1, name:'Tel Aviv'},{id:1, name:'London'},{id:1, name:'Paris'}];
  temperature:number;
  city:string; 

  onSubmit(){
    this.router.navigate(['/temperatures', this.temperature,this.city]);
  }


  ngOnInit() {
  }

}
