import { Injectable } from '@angular/core';
import {Observable} from 'rxjs'
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BooksService {

//books:any =  [{title:'Alice in Wonderland', author:'Lewis Carrol'},{title:'War and Peace', author:'Leo Tolstoy'}, {title:'The Magic Mountain', author:'Thomas Mann'}]


/*getBooks(){
  const booksObservable = new Observable(
    observer => {
      setInterval(() =>observer.next(this.books)
        ,5000
      )
      }
    )
    return booksObservable;
  }
  */
userCollection:AngularFirestoreCollection = this.db.collection('users');
bookCollection:AngularFirestoreCollection;

  getBooks(userId:string):Observable<any[]>{       
      //return this.db.collection('books').valueChanges({idField:'id'});
      this.bookCollection = this.db.collection(`users/${userId}/books`);
     return this.bookCollection.snapshotChanges().pipe(
     map(
        collection => collection.map(
          document => {
            const data = document.payload.doc.data();
            data.id = document.payload.doc.id;
            console.log(data);
            return data;
          }
        )
      )
    )
  }

 deleteBook(userId:string, id:string){
  this.db.doc(`users/${userId}/books/${id}`).delete();
}
updateBook(userId:string, id:string, title:string, author:string){
  this.db.doc(`users/${userId}/books/${id}`).update({
  title:title,
  author:author
  })
}
getBook(userId:string, id:string):Observable<any>{
  return this.db.doc(`users/${userId}/books/${id}`).get();
}

addBook(userId:string, title:string, author:string){
  const book = {title:title, author:author};
  //this.db.collection('books').add(book);
  this.userCollection.doc(userId).collection('books').add(book);
}

/*
addBooks(){
setInterval(
  () => this.books.push({title:'A new book', author:'New author'})
  ,5000)
}
*/
/*
  getBooks(){
setInterval(()=>this.books,1000)
  }
*/
  constructor(private db:AngularFirestore) { }
}
