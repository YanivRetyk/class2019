import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {Observable} from 'rxjs'
import { TempService } from '../temp.service';
import { Weather } from '../interfaces/weather';

@Component({
  selector: 'app-temperatures',
  templateUrl: './temperatures.component.html',
  styleUrls: ['./temperatures.component.css']
})
export class TemperaturesComponent implements OnInit {


 
  temperature; 
  city;
  image:String;

  tempData$:Observable<Weather>;

  likes:number = 0;
  addLikes(){
    this.likes++
  }

 constructor(private route: ActivatedRoute, private tempService:TempService) { }


  ngOnInit() {
    //this.temperature = this.route.snapshot.params.temp;
    this.city= this.route.snapshot.params.city;
    this.tempData$ = this.tempService.searchWeatherData(this.city);
    this.tempData$.subscribe(
      data => {
        console.log(data);
        this.temperature = data.temperature;
        this.image = data.image;
      }
    )
  }

}
