import { Router, ActivatedRoute } from '@angular/router';
import { BooksService } from './../books.service';
import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-bookform',
  templateUrl: './bookform.component.html',
  styleUrls: ['./bookform.component.css']
})
export class BookformComponent implements OnInit {

 constructor(private booksservice:BooksService, 
  private router:Router, 
  private route:ActivatedRoute,
  public authService:AuthService) { }
  
  author:string;
  title:string;
  id:string;
  isEdit:boolean = false;
  buttonText:string = "Add book";
  userId:string;

  onSubmit(){ 
if(this.isEdit){
this.booksservice.updateBook(this.userId, this.id, this.title, this.author)
}else{

    this.booksservice.addBook(this.userId, this.title,this.author)
}
    this.router.navigate(['/books']);
  }  
 

ngOnInit() {
  this.id = this.route.snapshot.params.id;
  this.authService.user.subscribe(
     user => {
        this.userId = user.uid;
        console.log(this.id);

      if(this.id){
        this.isEdit = true;
        this.buttonText = "Update";
        this.booksservice.getBook(this.userId, this.id).subscribe(
      book => {
        console.log(book.data().author)
         console.log(book.data().title)
         this.author = book.data().author;
         this.title = book.data().title;
       }
      )
    }
  }
)

  }

}
